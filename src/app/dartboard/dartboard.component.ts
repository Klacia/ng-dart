import { Component, Input } from '@angular/core';
import { Game } from "../shared/model/game";
import { DartHit } from "../shared/model/dart-hit";

@Component({
    selector: 'dartboard',
    templateUrl: './dartboard.component.html',
})
export class DartBoardComponent {
    @Input() game: Game;

    onSelect(event: any): void {

        console.log(" Hit" + event.srcElement.id);
        if (this.game.isStarted()) {
            this.game.updateState(new DartHit(event.srcElement.id));
        }
    }

    onMouseOver(event: any): void{
        event.srcElement.style.opacity="0.6"
    }

    onMouseLeave(event: any): void{
        event.srcElement.style.opacity="1.0"
    }

    nextPlayer(): void {
        console.log("nextPlayer");
        if (this.game.isStarted()) {
            this.game.nextPlayer();
        }
    }

    undo(): void {
        console.log("undo");
        if (this.game.isStarted()) {
            this.game.undo();
        }
    }

    missThrow(): void {
        console.log("missThrow");
        if (this.game.isStarted()) {
            this.game.updateState(new DartHit("miss"));
        }
    }

}
