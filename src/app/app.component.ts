import { Component, ViewChild } from '@angular/core';
import { Game } from './shared/model/game'
import { Player } from './shared/model/player';
import { ModalComponent } from './shared/_component/modal/modal.component';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
})
export class AppComponent {

    @ViewChild(ModalComponent)
    public readonly modal: ModalComponent;

    game: Game;
    players: Player[];
    newPlayerName: string;

    newGame(): void {
        console.log("New Game");

        this.players = [];
        
        this.modal.show();
    }

    startGame(): void {
        console.log("Start Game");

        if (this.players.length > 0) {
            this.modal.hide();
            this.game = new Game("Game 1", this.players);
            this.game.start();
        }
    }

    addPlayer(playerName: string): void {
        console.log("Add player " + playerName);

        let newPlayer: Player = new Player(playerName);
        this.players.push(newPlayer);
    }

    removePlayer(player: Player): void {
        console.log("Remove player " + player);

        let index: number = this.players.indexOf(player);
        if (index !== -1) {
            this.players.splice(index, 1);
        }
    }
}
