import { Component, Input } from '@angular/core';
import { Game } from "../shared/model/game";
import { Player } from '../shared/model/player';

@Component({
    selector: 'results',
    templateUrl: './results.component.html',
    styleUrls: [ './results.component.css' ]
})
export class ResultsComponent {
    @Input() game: Game;
    @Input() currentPlayer: Player;
    @Input() winnerPlayer:Player;
}
