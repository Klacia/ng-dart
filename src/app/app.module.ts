import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { DartBoardComponent }  from './dartboard/dartboard.component';
import { ResultsComponent }  from './results/results.component';
import { ModalComponent } from './shared/_component/modal/modal.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule],
  declarations: [ AppComponent, DartBoardComponent, ResultsComponent, ModalComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
