import { DartHit } from "./dart-hit";

export class Turn {

    points: number = 0;
    hits: DartHit[] = [];

    addHit(hit: DartHit){
		if (this.isFinished()){
			throw new Error("Player turn is already finished");
		}

		this.hits.push(hit);
		this.points += hit.points;
	}

    undoHit(): DartHit{
		if(this.isNotStarted()){
			throw new Error("Cannot undo hit! Turn is not started");
		}
		
		let undoHit: DartHit = this.hits.pop();
		this.points -= undoHit.points;	

        return undoHit;
	}
	
    isFinished(): boolean{
		return this.hits.length == 3;
	}
	
	isNotStarted(): boolean {
		return this.hits.length == 0;
	}
}