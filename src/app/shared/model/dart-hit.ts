import { Penalty } from './rules/penalty';

export class DartHit {

    points: number = 0;
    hit: string;
    hitField: string;
    penalty: Penalty;

    constructor(hit: string) {
        this.hit = hit;
        this.points = this.parseHitString(hit);
        this.hitField = this.parseHitField(hit);
    }

    private parseHitString(hit: string): number {
        let pts: number = 0;

        if (hit == "Bull") {
            pts = 50;
        } else if (hit == "Outer") {
            pts = 25;
        } else if (hit == "miss") {
            pts = 0;
        } else {
            pts = this.parseFactor(hit) * this.parsePoints(hit)
        }

        return pts;
    }

    private parseHitField(hit: string): string {
		if (hit.length == 3) {
			return hit.substring(1);
		} else {
			return hit;
		}
    }

    private parseFactor(hit: string): number {

        switch (hit.charAt(0)) {
            case "s":
                return 1;
            case "d":
                return 2;
            case "t":
                return 3;
            default:
                console.log("Unknown factor " + hit)
                return 0;
        }
    }

    private parsePoints(hit: string): number {
        let pts: string = hit.trim().substr(1, 2);
        if (isNaN(Number(pts))) {
            return 0;
        } else {
            return Number(pts);
        }
    }
}