import { GameRules } from './game-rules';
import { Game } from '../game';
import { DartHit } from '../dart-hit';
import { Penalty } from './penalty';

export class X01Rules implements GameRules {

    game: Game;
    initalPoints: number;

    constructor(game: Game, initalPoints: number) {
        this.game = game;
        this.initalPoints = initalPoints;
    }

    isGameFinished(): boolean {
        return this.game.isStarted() && this.game.currentPlayer.points == 0;
    }

    isHitValid(hit: DartHit): Penalty {
        hit.penalty = Penalty.NONE;
        if (this.game.currentPlayer.points - hit.points < 0) {
            hit.penalty = Penalty.END_ROUND;
            return Penalty.END_ROUND;
        }
        return Penalty.NONE;
    }
    
    getInitialPlayerPoints(): number {
        return this.initalPoints;
    }

    addPoints(hit: DartHit): number {
        return -hit.points
    }

}