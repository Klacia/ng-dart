import { GameType } from '../game-type';
import { DartHit } from '../dart-hit';
import { Penalty } from "./penalty";

export interface GameRules {

    isGameFinished(): boolean;

    isHitValid(hit: DartHit): Penalty;

    getInitialPlayerPoints(): number;

    addPoints(hit: DartHit): number;

}