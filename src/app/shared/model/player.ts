
import { Turn } from "./turn";
import { DartHit } from "./dart-hit";
import { GameRules } from './rules/game-rules';
import { Penalty } from './rules/penalty';

export class Player {
    name: string;
    points: number = 0;
    turns: Turn[] = [];

    constructor(name: string) {
        this.name = name;
    }

    /**
     * addHit
     */
    addHit(hit: DartHit, rules: GameRules): boolean {

        let penalty: Penalty = rules.isHitValid(hit);

        this.points += rules.addPoints(hit);

        let lastTurn: Turn = this.getLastTurn();

        if (!lastTurn.isFinished()) {
            lastTurn.addHit(hit);
        } else {
            throw new Error("Cannot add point to finished turn");
        }

        switch (penalty) {
            case Penalty.END_ROUND:
                this.removePointsFromCurrentTurn();
                return true;
        }

        return lastTurn.isFinished();
    }

    undoHit(rules: GameRules) {
        if (this.turns.length > 0) {
            let lastTurn: Turn = this.getLastTurn();
            let undoHit: DartHit = lastTurn.undoHit();

            switch (undoHit.penalty) {
                case Penalty.END_ROUND:
                    this.addPointsFromTurn();
                    break;
                default:
                    this.points -= rules.addPoints(undoHit);
            }

        }
    }

    public getLastTurn(): Turn {
        return this.turns[this.turns.length - 1];
    }

    addNewTurn(): void {
        this.turns.push(new Turn());
    }

    removeNotStartedTurn(): void {
        if (this.getLastTurn().isNotStarted()) {
            this.turns.pop();
        } else {
            throw new Error("Turn is already started");
        }
    }

    private removePointsFromCurrentTurn(): void {
        this.points += this.getLastTurn().points;
    }

    private addPointsFromTurn(): void {
        this.points -= this.getLastTurn().points;
    }
}