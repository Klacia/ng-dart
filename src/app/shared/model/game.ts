import { Player } from './player';
import { DartHit } from "./dart-hit";
import { GameRules } from './rules/game-rules';
import { X01Rules } from './rules/x01-rules';
import { Penalty } from './rules/penalty';

export class Game {

	id: string;
	players: Player[] = [];
	currentPlayer: Player;
	rules: GameRules;
	winnerPlayer: Player;

	constructor(id: string, playertab: Player[]) {
		this.id = id;
		this.players = playertab;
		this.rules = new X01Rules(this, 301);
		this.initGame();
	}

	private initGame(): void {
		// set initial points for players
		for (var i = 0; i < this.players.length; i++) {
			this.players[i].points = this.rules.getInitialPlayerPoints();
		}
	}

	public start(): void {
		this.winnerPlayer = null;

		if (this.players.length > 0) {
			this.currentPlayer = this.players[0];
			this.currentPlayer.addNewTurn();
		} else {
			console.log("Error: no players to start new game");
		}
	}

	public isStarted(): boolean {
		return this.currentPlayer != null;
	}

	public updateState(event: DartHit) {
		// this need refactoring
		if (this.rules.isGameFinished()) {
			this.winnerPlayer = this.currentPlayer;
		} else {

			let changePlayer = this.currentPlayer.addHit(event, this.rules)

			if (this.rules.isGameFinished()) {
				this.winnerPlayer = this.currentPlayer;
				return;
			}

			if (changePlayer) {
				this.nextPlayer();
			}

		}

	}

	public undo() {
		if (this.rules.isGameFinished()) {
			this.winnerPlayer = null;
		}

		if (this.currentPlayer.getLastTurn().isNotStarted()) {
			this.prevPlayer();
		}

		this.currentPlayer.undoHit(this.rules);

	}

	public nextPlayer() {
		let indexOfCurrPlayer: number = this.players.indexOf(this.currentPlayer);

		if (indexOfCurrPlayer == this.players.length - 1) {
			this.currentPlayer = this.players[0];
		} else {
			this.currentPlayer = this.players[++indexOfCurrPlayer];
		}

		this.currentPlayer.addNewTurn();
	}

	private prevPlayer() {

		this.currentPlayer.removeNotStartedTurn();

		let indexOfCurrPlayer: number = this.players.indexOf(this.currentPlayer);

		if (indexOfCurrPlayer == 0) {
			this.currentPlayer = this.players[this.players.length - 1];
		} else {
			this.currentPlayer = this.players[indexOfCurrPlayer - 1];
		}
	}
}