import { Game } from '../app/shared/model/game'
import { Player } from "../app/shared/model/player";
import { DartHit } from "../app/shared/model/dart-hit";

let p1: Player;
let p2: Player;
let p3: Player;

beforeEach(function () {
    p1 = new Player("p1");
    p2 = new Player("p2");
    p3 = new Player("p3");
});

describe('should start game', () => {

    it("with 2 players", function () {
        let game = new Game("g1", [p1, p2]);
        game.start();
        expect(game.currentPlayer).toBe(p1);
    });

});

describe('should not start game', () => {

    it("with no players", function () {
        let game = new Game("g2", []);
        try {
            game.start();
        } catch (error) {
            expect(error).not.toBeNull();
        }
    });

});


describe('should calculate points', () => {

    it("for 3 players", function () {
        let game = new Game("g3", [p1, p2, p3]);
        game.start();

        game.updateState(new DartHit("s1"));
        game.updateState(new DartHit("s2"));
        game.updateState(new DartHit("s3"));

        game.updateState(new DartHit("d1"));
        game.updateState(new DartHit("d2"));
        game.updateState(new DartHit("d3"));

        game.updateState(new DartHit("t1"));
        game.updateState(new DartHit("t2"));
        game.updateState(new DartHit("t3"));

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (1 + 2 + 3));
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints() - (2 + 4 + 6));
        expect(game.players[2].points).toBe(game.rules.getInitialPlayerPoints() - (3 + 6 + 9));

        game.updateState(new DartHit("Bull"));

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (1 + 2 + 3 + 50));
    });

    it("and select the winner player", function () {
        let game = new Game("g3", [p1, p2]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("d1"));
        game.updateState(new DartHit("d2"));
        game.updateState(new DartHit("d3"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("s1"));

        expect(game.players[0].points).toBe(0);
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints() - (2 + 4 + 6));
        expect(game.players[0]).toBe(game.winnerPlayer);

    });

    it("and end round without adding points when player score more than should", function () {
        let game = new Game("g3", [p1]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (60 + 60 + 60));
        expect(game.players[0].turns.length).toBe(3)

    });

});

describe('should undo hits', () => {

    it("for 3 players", function () {
        let game = new Game("g4", [p1, p2, p3]);
        game.start();

        game.updateState(new DartHit("s1"));
        game.updateState(new DartHit("s2"));
        game.updateState(new DartHit("s3"));

        game.updateState(new DartHit("d1"));
        game.updateState(new DartHit("d2"));
        game.updateState(new DartHit("d3"));

        game.updateState(new DartHit("t1"));
        game.updateState(new DartHit("t2"));
        game.updateState(new DartHit("t3"));

        game.undo()
        game.undo()
        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (1 + 2 + 3));
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints() - (2 + 4 + 6));
        expect(game.players[2].points).toBe(game.rules.getInitialPlayerPoints());

        game.undo()
        game.undo()
        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (1 + 2 + 3));
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[2].points).toBe(game.rules.getInitialPlayerPoints());

        game.undo()
        game.undo()
        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[2].points).toBe(game.rules.getInitialPlayerPoints());
    });

    it("and the winner player", function () {
        let game = new Game("g3", [p1, p2]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("d1"));
        game.updateState(new DartHit("d2"));
        game.updateState(new DartHit("d3"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("s1"));

        for (let i = 0; i < 9; i++) {
            game.undo()
        }

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[1].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.winnerPlayer).toBeNull();

    });

    it("from end round without adding points when player score more than should throw 3", function () {
        let game = new Game("g3", [p1]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints() - (60 + 60 + 60));
        expect(game.players[0].turns.length).toBe(3)

        game.undo()
        game.undo()
        game.undo()

        game.undo()
        game.undo()
        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[0].turns.length).toBe(1)

    });

    it("from end round without adding points when player score more than should throw 1", function () {
        let game = new Game("g44", [p1]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t10"));
        game.updateState(new DartHit("t10"));

        game.updateState(new DartHit("t20"));

        game.undo()
        game.undo()
        game.undo()

        game.undo()
        game.undo()
        game.undo()

        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[0].turns.length).toBe(1)

    });

    it("from end round without adding points when player score more than should throw 2", function () {
        let game = new Game("g4", [p1]);
        game.start();

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t20"));

        game.updateState(new DartHit("t20"));
        game.updateState(new DartHit("t10"));
        game.updateState(new DartHit("10"));

        game.updateState(new DartHit("20"));
        game.updateState(new DartHit("20"));

        game.undo()
        game.undo()
        game.undo()

        game.undo()
        game.undo()
        game.undo()

        game.undo()
        game.undo()

        expect(game.players[0].points).toBe(game.rules.getInitialPlayerPoints());
        expect(game.players[0].turns.length).toBe(1)

    });

});