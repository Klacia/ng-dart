"use strict";
var dart_hit_1 = require("../app/shared/dart-hit");
describe('should parse hit string correctly', function () {
    it('s10', function () { return expect(new dart_hit_1.DartHit("s10").points).toBe(10); });
    it('d20', function () { return expect(new dart_hit_1.DartHit("d20").points).toBe(40); });
    it('t20', function () { return expect(new dart_hit_1.DartHit("t20").points).toBe(60); });
    it('Bull', function () { return expect(new dart_hit_1.DartHit("Bull").points).toBe(50); });
    it('Outer', function () { return expect(new dart_hit_1.DartHit("Outer").points).toBe(25); });
    it('miss', function () { return expect(new dart_hit_1.DartHit("miss").points).toBe(0); });
    it('Legia', function () { return expect(new dart_hit_1.DartHit("Legia").points).toBe(0); });
});
//# sourceMappingURL=dart-hit.spec.js.map