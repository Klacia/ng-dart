import { DartHit } from '../app/shared/model/dart-hit'

describe('should parse hit string correctly', () => {

    it('s10', () => expect(new DartHit("s10").points).toBe(10));

    it('d20', () => expect(new DartHit("d20").points).toBe(40));

    it('t20', () => expect(new DartHit("t20").points).toBe(60));

    it('Bull', () => expect(new DartHit("Bull").points).toBe(50));

    it('Outer', () => expect(new DartHit("Outer").points).toBe(25));

    it('miss', () => expect(new DartHit("miss").points).toBe(0));

    it('Legia', () => expect(new DartHit("Legia").points).toBe(0));
});