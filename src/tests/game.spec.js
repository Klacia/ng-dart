"use strict";
var game_1 = require("../app/shared/game");
var player_1 = require("../app/shared/player");
var dart_hit_1 = require("../app/shared/dart-hit");
var p1;
var p2;
var p3;
beforeEach(function () {
    p1 = new player_1.Player("p1");
    p2 = new player_1.Player("p2");
    p3 = new player_1.Player("p3");
});
describe('should start game', function () {
    it("with 2 players", function () {
        var game = new game_1.Game("g1", [p1, p2]);
        game.start();
        expect(game.currentPlayer).toBe(p1);
    });
});
describe('should not start game', function () {
    it("with no players", function () {
        var game = new game_1.Game("g2", []);
        try {
            game.start();
        }
        catch (error) {
            expect(error).not.toBeNull();
        }
    });
});
describe('should calculate points', function () {
    it("for 3 players", function () {
        var game = new game_1.Game("g3", [p1, p2, p3]);
        game.start();
        game.updateState(new dart_hit_1.DartHit("s1"));
        game.updateState(new dart_hit_1.DartHit("s2"));
        game.updateState(new dart_hit_1.DartHit("s3"));
        game.updateState(new dart_hit_1.DartHit("d1"));
        game.updateState(new dart_hit_1.DartHit("d2"));
        game.updateState(new dart_hit_1.DartHit("d3"));
        game.updateState(new dart_hit_1.DartHit("t1"));
        game.updateState(new dart_hit_1.DartHit("t2"));
        game.updateState(new dart_hit_1.DartHit("t3"));
        expect(game.players[0].points).toBe(1 + 2 + 3);
        expect(game.players[1].points).toBe(2 + 4 + 6);
        expect(game.players[2].points).toBe(3 + 6 + 9);
        game.updateState(new dart_hit_1.DartHit("Bull"));
        expect(game.players[0].points).toBe(1 + 2 + 3 + 50);
    });
});
describe('should undo hits', function () {
    it("for 3 players", function () {
        var game = new game_1.Game("g4", [p1, p2, p3]);
        game.start();
        game.updateState(new dart_hit_1.DartHit("s1"));
        game.updateState(new dart_hit_1.DartHit("s2"));
        game.updateState(new dart_hit_1.DartHit("s3"));
        game.updateState(new dart_hit_1.DartHit("d1"));
        game.updateState(new dart_hit_1.DartHit("d2"));
        game.updateState(new dart_hit_1.DartHit("d3"));
        game.updateState(new dart_hit_1.DartHit("t1"));
        game.updateState(new dart_hit_1.DartHit("t2"));
        game.updateState(new dart_hit_1.DartHit("t3"));
        game.undo();
        game.undo();
        game.undo();
        expect(game.players[0].points).toBe(1 + 2 + 3);
        expect(game.players[1].points).toBe(2 + 4 + 6);
        expect(game.players[2].points).toBe(0);
        game.undo();
        game.undo();
        game.undo();
        expect(game.players[0].points).toBe(1 + 2 + 3);
        expect(game.players[1].points).toBe(0);
        expect(game.players[2].points).toBe(0);
        game.undo();
        game.undo();
        game.undo();
        expect(game.players[0].points).toBe(0);
        expect(game.players[1].points).toBe(0);
        expect(game.players[2].points).toBe(0);
    });
});
//# sourceMappingURL=game.spec.js.map